package com.example.shibeapp.di

import com.example.shibeapp.model.remote.DogeService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DogeServiceNetworkModule {

    private const val BASE_URL = "https://shibe.online"

    @Provides
    @Singleton
    fun providesDogeService(): DogeService {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit.create(DogeService::class.java)
    }
}