package com.example.shibeapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shibeapp.model.DogeRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DogeViewModel @Inject constructor(private val repo: DogeRepo) : ViewModel() {

    private val _stateList = MutableLiveData<List<String>>()
    val stateList: LiveData<List<String>> get() = _stateList

    fun getDoges() {
        viewModelScope.launch {
            val strings = repo.getDoges()
            _stateList.value = strings
        }
    }
}