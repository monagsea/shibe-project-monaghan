package com.example.shibeapp.model

import com.example.shibeapp.model.remote.DogeService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject


class DogeRepo @Inject constructor(private val service: DogeService) {

    suspend fun getDoges(): List<String> = withContext(Dispatchers.IO) {
        service.getDoges()
    }
}