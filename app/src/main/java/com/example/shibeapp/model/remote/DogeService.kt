package com.example.shibeapp.model.remote

import retrofit2.http.GET

interface DogeService {

    @GET("/api/shibes?count=100&urls=true&httpsUrls=true")
    suspend fun getDoges(): List<String>
}

