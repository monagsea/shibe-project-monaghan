package com.example.shibeapp.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.shibeapp.adapter.DogeAdapter
import com.example.shibeapp.databinding.FragmentDogeBinding
import com.example.shibeapp.viewmodel.DogeViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DogeFragment : Fragment() {
    private var _binding: FragmentDogeBinding? = null
    private val binding get() = _binding!!
    private val dogeViewModel by viewModels<DogeViewModel>()
    private val dogeAdapter by lazy { DogeAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDogeBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var state = "linear"

        fun displayDoge() =
            with(binding.rvList) {
                when (state) {
                    "linear" -> {
                        layoutManager =
                            LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
                    }
                    "grid" -> {
                        layoutManager =
                            GridLayoutManager(this.context, 3, LinearLayoutManager.HORIZONTAL, false)
                    }
                    "staggered" -> {
                        layoutManager =
                            StaggeredGridLayoutManager(3, LinearLayoutManager.HORIZONTAL)
                    }
                }
                    adapter = dogeAdapter.apply {
                        with(dogeViewModel) {
                            getDoges()
                            stateList.observe(viewLifecycleOwner) {
                                addDoges(it)
                            }
                        }
                    }
            }
        displayDoge()

        binding.linearBtn.setOnClickListener {
            state = "linear"
            displayDoge()
        }
        binding.gridBtn.setOnClickListener {
            state = "grid"
            displayDoge()
        }
        binding.staggeredGrid.setOnClickListener {
            state = "staggered"
            displayDoge()
        }
    }
}