package com.example.shibeapp.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shibeapp.databinding.ItemDogeBinding
import com.example.shibeapp.model.DogeRepo
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint

class DogeAdapter : RecyclerView.Adapter<DogeAdapter.DogeViewHolder>() {
    private var doges = listOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DogeViewHolder {
        val binding = ItemDogeBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return DogeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DogeViewHolder, position: Int) {
        val doge = doges[position]
        holder.loadDoge(doge)
    }

    override fun getItemCount(): Int {
        return doges.size
    }

    fun addDoges(doges: List<String>) {
        this.doges = doges.toMutableList()
        Log.e("test", "addDoges: $doges" )
        notifyDataSetChanged()
    }

    class DogeViewHolder(
        private val binding: ItemDogeBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadDoge(doge: String) {
            Picasso.get().load(doge).into(binding.ivDoge)
        }
    }
}